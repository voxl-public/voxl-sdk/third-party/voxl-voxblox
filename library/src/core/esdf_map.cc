#include "voxblox/core/esdf_map.h"

namespace voxblox
{
  bool EsdfMap::getDistanceAtPosition(const Point& position, float *distance,
                                      bool interpolate, bool use_hallucinated) const
  {
    bool success = interpolator_.getDistance(position, distance, interpolate, use_hallucinated);
    return success;
  }

  bool EsdfMap::getDistanceAndGradientAtPosition(
      const Point &position, float *distance, Point *gradient,
      bool interpolate, bool use_hallucinated) const
  {
    bool use_adaptive = false;
    bool success = false;
    if (use_adaptive)
      success = interpolator_.getAdaptiveDistanceAndGradient(position, distance, gradient);
    else
    {
      success = interpolator_.getDistance(position, distance, interpolate, use_hallucinated);
      success &= interpolator_.getGradient(position, gradient, interpolate);
    }

    return success;
  }

  bool EsdfMap::isObserved(const Point &position) const
  {
    // Get the block.
    Block<EsdfVoxel>::Ptr block_ptr =
        esdf_layer_->getBlockPtrByCoordinates(position);

    if (block_ptr)
    {
      const EsdfVoxel &voxel =
          block_ptr->getVoxelByCoordinates(position);
      return voxel.observed;
    }
    return false;
  }

  unsigned int EsdfMap::coordPlaneSliceGetDistance(
      unsigned int free_plane_index, double free_plane_val,
      EigenDRef<Eigen::Matrix<double, 3, Eigen::Dynamic>> &positions,
      Eigen::Ref<Eigen::VectorXd> distances, unsigned int max_points) const
  {
    BlockIndexList blocks;
    esdf_layer_->getAllAllocatedBlocks(&blocks);

    // Cache layer settings.
    size_t vps = esdf_layer_->voxels_per_side();
    size_t num_voxels_per_block = vps * vps * vps;

    // Temp variables.
    bool did_all_fit = true;
    unsigned int count = 0;

    // TODO(mereweth@jpl.nasa.gov) - store min/max index (per axis) allocated in
    // Layer This extra bookeeping will make this much faster
    for (const BlockIndex &index : blocks)
    {
      // Iterate over all voxels in said blocks.
      const Block<EsdfVoxel> &block = esdf_layer_->getBlockByIndex(index);

      Point origin = block.origin();
      if (std::abs(origin(free_plane_index) - free_plane_val) >
          block.block_size())
      {
        continue;
      }

      for (size_t linear_index = 0; linear_index < num_voxels_per_block;
           ++linear_index)
      {
        Point coord = block.computeCoordinatesFromLinearIndex(linear_index);
        const EsdfVoxel &voxel = block.getVoxelByLinearIndex(linear_index);
        if (std::abs(coord(free_plane_index) - free_plane_val) <=
            block.voxel_size())
        {
          double distance;
          if (voxel.observed)
          {
            distance = voxel.distance;
          }
          else
          {
            continue;
          }

          if (count < positions.cols())
          {
            positions.col(count) =
                Eigen::Vector3d(coord.x(), coord.y(), coord.z());
          }
          else
          {
            did_all_fit = false;
          }
          if (count < distances.size())
          {
            distances(count) = distance;
          }
          else
          {
            did_all_fit = false;
          }
          count++;
          if (count >= max_points)
          {
            return count;
          }
        }
      }
    }

    if (!did_all_fit)
    {
      throw std::runtime_error(std::string("Unable to store ") +
                               std::to_string(count) + " values.");
    }

    return count;
  }

} // namespace voxblox
